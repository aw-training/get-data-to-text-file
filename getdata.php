<?php
?>
<!DOCTYPE html>
<html>
  <head>
    <title></title>
    <style type="text/css">
      .border{
        border:1px solid black;
      }
      table{
        text-align: center;
      }
      img{
        width: 150px;
        height: 150px;
      }
      tr{
        width: 100%;
      }
      tr:nth-child(1){
        color:#24247d;
        font-size: 20px;
        font-weight: bold;
      }
      td:nth-child(1){
        width: 10%;
      }
      td:nth-child(2){
        width: 20%;
      }
      td:nth-child(3){
        width: 10%;
      }
      td:nth-child(4){
        width: 30%;
      }
      td:nth-child(5){
        width: 10%;
      }
      td:nth-child(6){
        width: 20%;
      }
      .top-button a:link, a:visited {
        width: 200px;
        background-color:#91d667;
        color: #f7faf5;
        padding: 15px 25px;
        font-size: 20px;
        font-weight: bold;
        border-radius:3px;
        border:1px solid black;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        margin-bottom: 40px;
      }
      .top-button a:hover, a:active {
        background-color: #79b555;
      }
    </style>
  </head>
<body>
<div class="top-button">
  <a href="index.html">Add More People</a>
</div>
<?php
$filename='data.txt';
$c = count(file($filename)); 

$k=filesize($filename);
$myfile = fopen($filename,"r");
echo "<table cellspacing=0>";
for ($i=0; $i < $c/6; $i++){
  echo "<tr>";
  for ($j=0; $j < 6; $j++){
    echo "<td class=border>";
    $line = fgets($myfile, filesize($filename));
    if(($j%6)==3){
      if ($i==0) {
        echo $line;
      }
      else{
        echo '<img src="'.$line.'" alt="">';
      }
    }
    else{
    echo $line;
    }
    echo "</td>";
  }
  echo "</tr>";
}
echo "</table>";
fclose($myfile);
?>
</body>
</html>